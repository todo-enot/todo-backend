# todo-backend

Список сохраняется по user-agent

## Getting started

```bash
# install dependencies
$ pip install -r requirements.txt

# activate venv:
$ cd /venv/Scripts
$ activate

# db work
$ python src/manage.py migrate
$ python src/manage.py createsuperuser

# serve with hot reload at localhost:8000
$ python src/manage.py runserver
```