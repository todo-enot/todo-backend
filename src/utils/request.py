from typing import Any

from api.exceptions import IncorrectField


def get_field_or_exception(data_source: dict, name: str, pop: bool = False) -> Any:
    value = data_source.get(name)
    if not value:
        raise IncorrectField(f"Field {name} is not provided")
    if pop:
        del data_source[name]
    return value