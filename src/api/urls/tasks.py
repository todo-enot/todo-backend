from rest_framework.routers import DefaultRouter

from api.views.tasks import Tasks_TasksReadViewSet

router = DefaultRouter()
router.register("tasks",Tasks_TasksReadViewSet, "task-tasks")
urls = []

urls.extend([*router.urls])