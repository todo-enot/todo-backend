from django.urls import path

from api.views.fragments.file_fragment import TaskFragment_TaskFragmentFileViewSet
from api.views.fragments.fragment import TaskFragment_TaskFragmentViewSet
from api.views.fragments.image_fragment import TaskFragment_TaskFragmentImageViewSet

urls = [
    path("task/fragment/fragments/",
         TaskFragment_TaskFragmentViewSet.as_view({'post': "create", "put": "update", "delete": "delete_multiple"}),
         name="task-fragment-fragments"),
    path("task/fragment/image/",
         TaskFragment_TaskFragmentImageViewSet.as_view({'post': "create"}),
         name="task-fragment-image"),
    path("task/fragment/file/",
         TaskFragment_TaskFragmentFileViewSet.as_view({'post': "create"}),
         name="task-fragment-file")
]