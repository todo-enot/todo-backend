from rest_framework.routers import DefaultRouter

from api.views.task_item import TaskItem_TaskItemWriteViewSet

router = DefaultRouter()
router.register("task_item", TaskItem_TaskItemWriteViewSet, "task_item")

urls = []

urls.extend([*router.urls])