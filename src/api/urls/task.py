from rest_framework.routers import DefaultRouter

from api.views.tasks import Task_TaskWriteViewSet

router = DefaultRouter()
router.register("task", Task_TaskWriteViewSet, "task")

urls = []

urls.extend([*router.urls])