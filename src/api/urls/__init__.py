from django.urls import path, re_path

from api.swagger import schema_view
from .tasks import urls as tasks_urls
from .task import urls as task_urls
from .task_item import urls as task_item_urls
from .user import urls as user_urls
from .auth import urls as auth_urls
from .fragments import urls as fragments_urls

urlpatterns = [
    path(
        "swagger/",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    re_path(
        r"^swagger(?P<format>\.json|\.yaml)$",
        schema_view.without_ui(cache_timeout=0),
        name="schema-json",
    ),
    path(
        "redoc/",
        schema_view.with_ui("redoc", cache_timeout=0),
        name="schema-redoc",
    ),
]

urlpatterns.extend(tasks_urls)
urlpatterns.extend(task_urls)
urlpatterns.extend(task_item_urls)
urlpatterns.extend(user_urls)
urlpatterns.extend(auth_urls)
urlpatterns.extend(fragments_urls)
