from rest_framework import serializers


class TaskItem_TaskItemDeleteSerializer(serializers.Serializer):
    task_items_ids = serializers.ListSerializer(
        child=serializers.IntegerField(),
        required=True
    )

    task_id = serializers.IntegerField(required=True)