from rest_framework import serializers

from api.models import Task, TaskItem, TaskFragment
from api.serializers.fragments.fragment import Tasks_FragmentsReadSerializer


class TaskItem_TaskItemReadSerializer(serializers.ModelSerializer):

    class Meta:
        model = TaskItem
        fields = (
            "id",
            "index",
            "title",
            "text",
            "color",
            "is_done",
        )
        read_only_fields = fields


class Tasks_TasksReadSerializer(serializers.ModelSerializer):
    fragment_task = Tasks_FragmentsReadSerializer(many=True, read_only=True)

    class Meta:
        model = Task
        fields = (
            "id",
            "task_date",
            "type",
            "title",
            "fragment_task"
        )
        read_only_fields = fields
        prefetch_related = ("fragment_task",)
