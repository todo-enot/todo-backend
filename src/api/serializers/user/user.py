from rest_framework import serializers

from api.models import User


class User_UserReadSerializer(serializers.ModelSerializer):
    """Serializer for getting user"""

    class Meta:
        model = User
        fields = [
            "id",
            "email",
            "is_activated",
            "is_staff",
            "created_at",
        ]
        read_only_fields = fields


class User_UserUpdateSerializer(serializers.ModelSerializer):
    """Serializer for update user"""

    class Meta:
        model = User
        fields = [
            "email",
        ]


class User_EmailUpdateSerializer(serializers.ModelSerializer):
    confirmation_code = serializers.CharField(read_only=True)

    class Meta:
        model = User
        fields = ["email", "confirmation_code"]
