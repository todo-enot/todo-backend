from django.db import transaction
from rest_framework import serializers

from api.enums import TaskFragmentType
from api.models import TaskFragment, Fragment, FragmentImage, Task, CheckboxFragment, FragmentFile
from api.serializers.fields.stdimage import StdImageSerializerField


class Fragment_FragmentImageReadSerializer(serializers.ModelSerializer):
    image = StdImageSerializerField(read_only=True)

    class Meta:
        model = FragmentImage
        fields = ("image", "id")
        read_only_fields = fields


class Fragment_FragmentImageWriteSerializer(serializers.ModelSerializer):
    image = StdImageSerializerField(write_only=True)
    left = serializers.FloatField(required=False, read_only=True)
    top = serializers.FloatField(required=False, read_only=True)
    right = serializers.FloatField(required=False, read_only=True)
    bottom = serializers.FloatField(required=False, read_only=True)
    task_id = serializers.IntegerField(required=True)
    type = serializers.CharField(max_length=32)
    index = serializers.IntegerField(required=True)

    class Meta:
        model = FragmentImage
        fields = ("image", "left",
                  "top",
                  "right",
                  "bottom", "task_id", "type", "index")

    @staticmethod
    def _add_fragment_image(image, fragment):
        fragment_dict = {
            "type": fragment["type"],
            "index": fragment["index"]
        }

        image = FragmentImage.objects.create(image=image)
        fragment_create = Fragment.objects.create(image=image)
        fragment_dict["fragment"] = fragment_create

        return fragment_dict

    def create(self, validated_data):
        with transaction.atomic():
            article_id = validated_data.pop('task_id')
            task_instance = Task.objects.filter(id=article_id).first()
            if "image" in validated_data:
                image = validated_data.pop("image")
                fragment_info = {
                    "type": validated_data.pop("type"),
                    "index": validated_data.pop("index")
                }
                fragment = self._add_fragment_image(image, fragment_info)

            article_fragment_instance = TaskFragment.objects.create(type=fragment["type"], task=task_instance,
                                                                    fragment=fragment["fragment"],
                                                                    index=fragment["index"])
        return article_fragment_instance


class Fragment_FragmentCheckboxReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = CheckboxFragment
        fields = (
            "text",
            "color",
            "is_done"
        )


class Fragment_FragmentFileReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = FragmentFile
        fields = (
            "name",
            "file"
        )

class Fragment_FragmentFileWriteSerializer(serializers.ModelSerializer):
    task_id = serializers.IntegerField(required=True)
    type = serializers.CharField(max_length=32)
    index = serializers.IntegerField(required=True)

    class Meta:
        model = FragmentFile
        fields = (
            "name",
            "file",
            "task_id",
            "type",
            "index"
        )

    @staticmethod
    def _add_fragment_file(file, fragment):
        fragment_dict = {
            "type": fragment["type"],
            "index": fragment["index"]
        }

        image = FragmentFile.objects.create(file=file, name=fragment["name"])
        fragment_create = Fragment.objects.create(file=image)
        fragment_dict["fragment"] = fragment_create

        return fragment_dict

    def create(self, validated_data):
        with transaction.atomic():
            task_id = validated_data.pop('task_id')
            task_instance = Task.objects.filter(id=task_id).first()
            if "file" in validated_data:
                image = validated_data.pop("file")
                fragment_info = {
                    "type": validated_data.pop("type"),
                    "name": validated_data.pop("name"),
                    "index": validated_data.pop("index")
                }
                fragment = self._add_fragment_file(image, fragment_info)

            task_fragment_instance = TaskFragment.objects.create(type=fragment["type"], task=task_instance,
                                                                    fragment=fragment["fragment"],
                                                                    index=fragment["index"])
        return task_fragment_instance


class Fragment_FragmentReadSerializer(serializers.ModelSerializer):
    image = Fragment_FragmentImageReadSerializer(read_only=True)
    checkbox = Fragment_FragmentCheckboxReadSerializer(read_only=True)
    file = Fragment_FragmentFileReadSerializer(read_only=True)

    class Meta:
        model = Fragment
        fields = (
            "text",
            "checkbox",
            "image",
            "file",
        )


class Fragment_FragmentWriteSerializer(serializers.ModelSerializer):
    checkbox = Fragment_FragmentCheckboxReadSerializer(required=False)
    type = serializers.CharField(max_length=128, required=True)
    index = serializers.IntegerField(required=True)

    class Meta:
        model = Fragment
        fields = (
            "text",
            "checkbox",
            "type",
            "index",
        )

class Tasks_FragmentsReadSerializer(serializers.ModelSerializer):
    fragment = Fragment_FragmentReadSerializer(read_only=True)

    class Meta:
        model = TaskFragment
        fields = (
            "pk",
            "type",
            "fragment",
            "index",
        )

class Tasks_FragmentsWriteSerializer(serializers.ModelSerializer):
    fragments = serializers.ListField(
        child=Fragment_FragmentWriteSerializer(write_only=True),
        required=True
    )

    task_id = serializers.IntegerField(required=True)

    class Meta:
        model = TaskFragment
        fields = (
            "fragments",
            "task_id"
        )

    @staticmethod
    def _add_fragment(fragments):
        task_fragment_data = []
        for fragment in fragments:
            fragment_dict = {
                "type": fragment["type"],
                "index": fragment["index"]
            }
            if fragment["type"] == "text":
                fragment_create = Fragment.objects.create(text=fragment["text"])
                fragment_dict["fragment"] = fragment_create
            elif fragment["type"] == "checkbox":
                checkbox = CheckboxFragment.objects.create(**fragment["checkbox"])
                fragment_create = Fragment.objects.create(checkbox=checkbox)
                fragment_dict["fragment"] = fragment_create
            task_fragment_data.append(fragment_dict)

        return task_fragment_data

    @staticmethod
    def _add_update_task_fragments(fragments, task):
        many_fragments = []

        for ind, fragment in enumerate(fragments):
            many_fragments.append(
                TaskFragment(type=fragment["type"], task=task,
                                fragment=fragment["fragment"],
                                index=fragment["index"])
            )
        TaskFragment.objects.bulk_create(many_fragments)

    def create(self, validated_data):
        with transaction.atomic():
            create_fragments = []
            task_id = validated_data.pop('task_id')
            task_instance = Task.objects.filter(id=task_id).first()
            if "fragments" in validated_data:
                fragments = validated_data.pop("fragments")
                create_fragments = self._add_fragment(fragments)

            if len(create_fragments) > 0:
                self._add_update_task_fragments(create_fragments, task_instance)
            return fragments


class Tasks_TasksFragmentsUpdateSerializer(serializers.Serializer):
    fragments_id = serializers.ListField(
        child=serializers.IntegerField(),
        required=True
    )

    fragments = serializers.ListField(
        child=Fragment_FragmentWriteSerializer(),
        required=True
    )

    task_id = serializers.IntegerField(required=True)

    def update(self, instance, validated_data):
        with transaction.atomic():
            fragments_id = validated_data.pop("fragments_id")
            fragments_to_update = validated_data.pop("fragments")
            fragments = TaskFragment.not_deleted.filter(pk__in=fragments_id).select_related("fragment")
            fragments.update(index=None)

            for id, fragment in enumerate(fragments):
                fragment.index = fragments_to_update[id]["index"]
                fragment_instance = Fragment.objects.get(id=fragment.fragment.id)

                if fragment.type == "text":
                    fragment_instance.text = fragments_to_update[id]["text"]
                elif fragment.type == "checkbox":
                    checkbox = CheckboxFragment.objects.get(pk=fragments_to_update[id].checkbox.checkbox_id)
                    checkbox.color = fragments_to_update[id].checkbox.color
                    checkbox.text = fragments_to_update[id].checkbox.text
                    checkbox.is_done = fragments_to_update[id].checkbox.is_done

                    checkbox.save()
                fragment_instance.save()
                fragment.save()

            return fragments_id
