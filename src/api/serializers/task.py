from django.db import transaction
from rest_framework import serializers

from api.models import Task, TaskItem


class TaskItem_TaskItemUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskItem
        fields = (
            "is_done",
        )


class TaskItem_TaskItemWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskItem
        fields = (
            "index",
            "title",
            "text",
            "color",
            "is_done",
        )


class Task_TaskWriteSerializer(serializers.ModelSerializer):
    title = serializers.CharField(max_length=256, required=True)

    class Meta:
        model = Task
        fields = (
            "task_date",
            "title",
            "type",
            "is_done",
        )

    def create(self, validated_data):
        with transaction.atomic():
            request = self.context["request"]
            validated_data["owner"] = request.headers['User-Agent']
            validated_data["user"] = request.user
            instance = super().create(validated_data)
        return instance


class Task_TaskUpdateSerializer(serializers.ModelSerializer):
    title = serializers.CharField(max_length=256, required=True)

    class Meta:
        model = Task
        fields = (
            "task_date",
            "title",
            "is_done",
        )

    def update(self, instance, validated_data):
        with transaction.atomic():
            instance.title = validated_data.pop("title")
            instance.is_done = validated_data.pop("is_done")
            instance.task_date = validated_data.pop("task_date")
            instance.save()
        return instance
