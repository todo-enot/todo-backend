from django.db import transaction
from drf_yasg.utils import swagger_auto_schema
from rest_framework import viewsets, mixins, status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from api.models import Task, TaskItem
from api.serializers.task import TaskItem_TaskItemUpdateSerializer
from api.serializers.task_item import TaskItem_TaskItemDeleteSerializer
from utils.request import get_field_or_exception


class TaskItem_TaskItemWriteViewSet(viewsets.GenericViewSet,
                                    mixins.UpdateModelMixin,
                                    mixins.DestroyModelMixin):

    def get_serializer_class(self):
        if self.action in ('update'):
            return TaskItem_TaskItemUpdateSerializer

    lookup_url_kwarg = "id"

    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        task_item_id = self.kwargs.get(self.lookup_url_kwarg)
        return TaskItem.not_deleted.filter(pk=task_item_id)

    @swagger_auto_schema(
        methods=["DELETE"],
        request_body=TaskItem_TaskItemDeleteSerializer
    )
    @transaction.atomic
    @action(methods=["DELETE"], detail=False)
    def delete_multiple(self, request):
        task_items_ids = get_field_or_exception(request.data, "task_items_ids")
        task_id = get_field_or_exception(request.data, "task_id")
        owner = self.request.user
        task_instance = Task.not_delete_and_yesterday.filter(user=owner, pk=task_id).first()
        instances = TaskItem.not_deleted.filter(task=task_instance).select_for_update().filter(
            id__in=task_items_ids)
        instances.update(is_deleted=True, index=None)
        return Response(status=status.HTTP_204_NO_CONTENT)
