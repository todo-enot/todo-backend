from django.db.models import Prefetch
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, mixins, status
from rest_framework.filters import SearchFilter
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from api.filters.task import TaskFilterSet
from api.models import Task, TaskFragment
from api.pagination import StrictLimitOffsetPagination
from api.serializers.task import Task_TaskWriteSerializer, Task_TaskUpdateSerializer
from api.serializers.tasks import Tasks_TasksReadSerializer


class Tasks_TasksReadViewSet(viewsets.GenericViewSet,
                             mixins.ListModelMixin,
                             mixins.RetrieveModelMixin):

    pagination_class = StrictLimitOffsetPagination
    permission_classes = [IsAuthenticated]
    serializer_class = Tasks_TasksReadSerializer

    search_fields = ["title"]

    filter_backends = (
        SearchFilter,
        DjangoFilterBackend,
    )

    filterset_class = TaskFilterSet

    def get_queryset(self):
        return Task.not_delete_and_yesterday.filter(user=self.request.user).prefetch_related(
            Prefetch(
                "fragment_task",
                queryset=TaskFragment.not_deleted.all().select_related("fragment")
            )
        )

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(queryset)

        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class Task_TaskWriteViewSet(viewsets.GenericViewSet,
                            mixins.CreateModelMixin,
                            mixins.UpdateModelMixin,
                            mixins.DestroyModelMixin):
    permission_classes = [IsAuthenticated]

    lookup_url_kwarg = "task_id"

    def get_queryset(self):
        task_id = self.kwargs.get(self.lookup_url_kwarg)
        user = self.request.user

        return Task.not_delete_and_yesterday.filter(user=user, pk=task_id)

    def get_serializer_class(self):
        if self.action in ('create',):
            return Task_TaskWriteSerializer
        elif self.action in ('update',):
            return Task_TaskUpdateSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=self.request.data)
        data = self.request.data
        serializer.is_valid(raise_exception=True)

        if serializer.is_valid():
            obj = serializer.save()

            headers = self.get_success_headers(data)
            return Response(
                {"id": obj.id},
                status=status.HTTP_201_CREATED,
                headers=headers,
            )
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, *args, **kwargs):
        task = self.get_queryset().first()
        partial = kwargs.pop("partial", False)
        serializer = self.get_serializer(task, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def destroy(self, request, *args, **kwargs):
        task = self.get_queryset().first()
        task_items = TaskFragment.objects.filter(task=task)
        task_items.update(is_deleted=True)

        return super().destroy(request, *args, **kwargs)