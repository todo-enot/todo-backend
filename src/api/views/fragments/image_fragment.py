from rest_framework import mixins, viewsets, status
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from api.serializers.fragments.fragment import Fragment_FragmentImageWriteSerializer
from api.services.helpers import crop_image


class TaskFragment_TaskFragmentImageViewSet(viewsets.GenericViewSet,
                            mixins.CreateModelMixin):
    permission_classes = [IsAuthenticated]
    parser_classes = [MultiPartParser]

    serializer_class = Fragment_FragmentImageWriteSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=self.request.data)
        data = self.request.data
        if all(
                key in data.keys() for key in ["left", "right", "top", "bottom", "image"]
        ):
            data["image"] = crop_image(
                float(data["left"]),
                float(data["top"]),
                float(data["right"]),
                float(data["bottom"]),
                data["image"],
            ).open()
        serializer.is_valid(raise_exception=True)

        self.perform_create(serializer)

        headers = self.get_success_headers(data)
        return Response(
            status=status.HTTP_201_CREATED,
            headers=headers,
        )


