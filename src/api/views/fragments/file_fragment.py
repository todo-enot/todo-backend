from rest_framework import viewsets, mixins, status
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from api.serializers.fragments.fragment import Fragment_FragmentFileReadSerializer, Fragment_FragmentFileWriteSerializer


class TaskFragment_TaskFragmentFileViewSet(viewsets.GenericViewSet,
                                           mixins.CreateModelMixin):
    permission_classes = [IsAuthenticated]
    parser_classes = [MultiPartParser]

    serializer_class = Fragment_FragmentFileWriteSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=self.request.data)
        data = self.request.data
        serializer.is_valid(raise_exception=True)

        self.perform_create(serializer)

        headers = self.get_success_headers(data)
        return Response(
            status=status.HTTP_201_CREATED,
            headers=headers,
        )
