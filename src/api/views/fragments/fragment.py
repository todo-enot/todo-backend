from django.db import transaction
from drf_yasg.utils import swagger_auto_schema
from rest_framework import viewsets, mixins, status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from api.models import Task, TaskFragment
from api.serializers.fragments.fragment import Tasks_FragmentsWriteSerializer, Tasks_TasksFragmentsUpdateSerializer
from utils.request import get_field_or_exception


class TaskFragment_TaskFragmentViewSet(viewsets.GenericViewSet,
                                       mixins.CreateModelMixin, mixins.UpdateModelMixin):
    permission_classes = [IsAuthenticated]

    lookup_url_kwarg = "task_id"

    def get_serializer_class(self):
        if self.action in ("create",):
            return Tasks_FragmentsWriteSerializer
        if self.action in ("update",):
            return Tasks_TasksFragmentsUpdateSerializer

    def get_queryset(self):
        task_id = self.kwargs.get(self.lookup_url_kwarg)
        task_instance = Task.not_delete_and_yesterday.filter(user=self.request.user, pk=task_id).first()
        if self.action in ("update",) and task_instance is not None:
            return TaskFragment.not_deleted.filter(task=task_instance)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=self.request.data)
        data = self.request.data
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        headers = self.get_success_headers(data)
        return Response(
            status=status.HTTP_201_CREATED,
            headers=headers,
        )

    def update(self, request, *args, **kwargs):
        task_id = self.kwargs.get(self.lookup_url_kwarg)
        task_instance = Task.not_delete_and_yesterday.filter(pk=task_id).first()
        fragments_id = request.data["fragments_id"]
        fragments = TaskFragment.not_deleted.filter(id__in=fragments_id, task=task_instance).select_related("fragment")
        partial = kwargs.pop("partial", False)
        serializer = self.get_serializer(fragments, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=status.HTTP_201_CREATED)

    @swagger_auto_schema()
    @transaction.atomic
    @action(methods=["DELETE"], detail=False)
    def delete_multiple(self, request):
        task_fragments_ids = get_field_or_exception(request.data, "task_fragments_ids")
        task_id = get_field_or_exception(request.data, "task_id")
        task_instance = Task.not_delete_and_yesterday.filter(user=self.request.user, pk=task_id).first()
        instances = TaskFragment.not_deleted.filter(task=task_instance).select_for_update().filter(
            id__in=task_fragments_ids)
        instances.update(is_deleted=True, index=None)
        return Response(status=status.HTTP_204_NO_CONTENT)
