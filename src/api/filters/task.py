from django_filters.rest_framework import FilterSet, filters
from django_filters.widgets import CSVWidget
from drf_yasg.openapi import Parameter

from api.models import Task


class BaseTaskFilterSet(FilterSet):
    class Meta:
        model = Task
        fields = ("id",)

class TaskFilterSet(BaseTaskFilterSet):

    def _sort_by_date_added(self, queryset, value):
        if not value:
            return queryset

        return queryset.order_by("-created_at")

    def _filter_by_date(self, queryset, name, value):
        if not value and name != 'date':
            return queryset
        return queryset.filter(task_date=value)

    def _filter_by_type(self, queryset, name, value):
        if not value and name != 'type':
            return queryset
        return queryset.filter(type=value)

    sort_newest = filters.BooleanFilter(
        method="_sort_by_date_added",
        help_text="Ordering by date article added: [true, false]",
    )

    sort_date = filters.BaseCSVFilter(
        distinct=True, widget=CSVWidget(), method="_filter_by_date"
    )

    filter_type = filters.CharFilter(method="_filter_by_type", distinct=True)