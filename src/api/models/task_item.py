from django.db import models
from django.db.models import UniqueConstraint

from api.models.abstract import BaseModel
from api.models.managers import NotDeletedManager


class TaskItem(BaseModel):
    index = models.PositiveIntegerField(null=True, blank=True)
    task = models.ForeignKey("api.Task", on_delete=models.PROTECT, null=True, blank=True, related_name="task_items")
    title = models.CharField(max_length=256)
    text = models.TextField(max_length=1000)
    color = models.CharField(max_length=64, null=True, blank=True)
    is_done = models.BooleanField(default=False)
    is_deleted = models.BooleanField(default=False)

    objects = models.Manager()
    not_deleted = NotDeletedManager()

    def delete(self, *args, **kwargs):
        self.is_deleted = True
        self.save()

    class Meta:
        verbose_name_plural = "TaskItem"

