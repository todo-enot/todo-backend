from django.db import models

from api.enums import TaskType
from api.models.abstract import BaseModel
from api.models.managers import NotYesterdayAndDeleteManager


class Task(BaseModel):
    owner = models.CharField(max_length=512, null=False, blank=False)
    user = models.ForeignKey("api.User", on_delete=models.PROTECT,
                             related_name="article_author",null=True, blank=True)
    task_date = models.DateField(auto_now=False, null=False)
    title = models.CharField(max_length=256, null=True, blank=True)
    type = models.CharField(max_length=32, choices=TaskType.choices, default=TaskType.note)
    is_deleted = models.BooleanField(default=False)
    is_done = models.BooleanField(default=False)

    objects = models.Manager()
    not_delete_and_yesterday = NotYesterdayAndDeleteManager()

    def delete(self, *args, **kwargs):
        self.is_deleted = True
        self.save()

    def __str__(self):
        return f"{self.pk}"

    class Meta:
        ordering = ['task_date']
        verbose_name_plural = "TaskInfo"