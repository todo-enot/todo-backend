import os
from datetime import datetime

from django.utils.translation import gettext_lazy as _
from stdimage import StdImageField

from api.models.abstract import BaseModel
from utils.image import image_processor


def get_fragment_image_path(
        instance, filename: str
) -> str:
    _, ext = os.path.splitext(filename)
    new_filename = f"{instance.pk}-{datetime.utcnow().strftime('%Y-%m-%d_%H-%M-%S')}"
    if ext:
        new_filename += ext
    return os.path.join(
        f"fragment/images/{instance.created_at.strftime('%Y/%m/%d')}/",
        new_filename,
    )


class FragmentImage(BaseModel):
    image = StdImageField(
        upload_to=get_fragment_image_path,
        verbose_name=_("Fragment image"),
        render_variations=image_processor,
        variations={
            "small": (328, 260),
            "medium": (800, 800),
            "large": (1600, 1600),
        },
        null=True,
        blank=True,
    )
