from django.db import models


class CheckboxFragment(models.Model):
    text = models.TextField(max_length=1000)
    color = models.CharField(max_length=64, null=True, blank=True)
    is_done = models.BooleanField(default=False)