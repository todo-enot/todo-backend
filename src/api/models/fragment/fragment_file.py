from django.db import models

from api.models.abstract import BaseModel
from api.models.storage import get_file_path_with_folder_name


def get_file_path(instance, filename):
    return get_file_path_with_folder_name(instance, filename, folder_name="files")


class FragmentFile(BaseModel):
    name = models.CharField(max_length=256, null=True, blank=True)
    file = models.FileField(upload_to=get_file_path, verbose_name="Original file")