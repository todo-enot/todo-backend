from django.db import models

from api.models.abstract import BaseModel


class Fragment (BaseModel):
    text = models.TextField(max_length=2000, null=True, blank=True)
    checkbox = models.ForeignKey("api.CheckboxFragment", on_delete=models.PROTECT, null=True, blank=True)
    image = models.ForeignKey("api.FragmentImage", on_delete=models.PROTECT, null=True, blank=True)
    file = models.ForeignKey("api.FragmentFile", on_delete=models.PROTECT, null=True, blank=True)

    def __str__(self):
        return f"{self.pk}"
