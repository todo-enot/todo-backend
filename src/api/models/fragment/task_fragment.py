from django.db import models
from django.db.models import UniqueConstraint

from api.enums import TaskFragmentType
from api.models.abstract import BaseModel
from api.models.managers import NotDeletedManager


class TaskFragment (BaseModel):
    index = models.PositiveIntegerField(null=True, blank=True)
    type = models.CharField(max_length=32, choices=TaskFragmentType.choices, default=TaskFragmentType.text)
    task = models.ForeignKey("api.Task", on_delete=models.PROTECT, related_name="fragment_task")
    fragment = models.ForeignKey("api.Fragment", on_delete=models.PROTECT, related_name="type_fragment", null=True, blank=True)

    is_deleted = models.BooleanField(default=False)

    objects = models.Manager()
    not_deleted = NotDeletedManager()

    def delete(self, *args, **kwargs):
        self.index = None
        self.is_deleted = True
        self.save()

    class Meta:
        verbose_name_plural = "TaskFragment"
        constraints = [
            UniqueConstraint(
                fields=("index", "task",),
                name="unique_index_and_task"
            )
        ]
        ordering = ['index']