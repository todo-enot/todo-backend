from django.contrib import admin


class TaskItemAdmin(admin.ModelAdmin):
    list_display = ("id","title", "index", "is_done", "task")

    search_fields = (
        "title",
    )

    list_filter = ("is_deleted",)