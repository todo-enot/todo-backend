from django.contrib.admin import ModelAdmin


class TaskFragmentAdmin(ModelAdmin):
    list_display = ("id", "type",)


class CheckboxFragmentAdmin(ModelAdmin):
    list_display = ("id", "text", "is_done", "color")


class FragmentAdmin(ModelAdmin):
    list_display = ("id", "text")


class FragmentImageAdmin(ModelAdmin):
    list_display = ("id", "image",)

class FragmentFileAdmin(ModelAdmin):
    list_display = ("id", "file", "name")