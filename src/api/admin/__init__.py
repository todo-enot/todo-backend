from django.contrib.admin import AdminSite

from api.admin.fragments import TaskFragmentAdmin, CheckboxFragmentAdmin, FragmentFileAdmin, FragmentImageAdmin, \
    FragmentAdmin
from api.admin.task import TaskAdmin
from api.admin.task_item import TaskItemAdmin
from api.admin.user import UserAdmin
from api.models import Task, TaskItem, User, TaskFragment, CheckboxFragment, FragmentFile, FragmentImage, Fragment

admin_site = AdminSite()

admin_site.register(Task, TaskAdmin)
admin_site.register(TaskItem, TaskItemAdmin)
admin_site.register(User, UserAdmin)
admin_site.register(TaskFragment, TaskFragmentAdmin)
admin_site.register(CheckboxFragment, CheckboxFragmentAdmin)
admin_site.register(FragmentFile, FragmentFileAdmin)
admin_site.register(FragmentImage, FragmentImageAdmin)
admin_site.register(Fragment, FragmentAdmin)