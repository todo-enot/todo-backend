from django.contrib import admin


class TaskAdmin(admin.ModelAdmin):
    list_display = ("id", "user", "owner", "task_date", "title",)
    list_filter = ("task_date", "is_deleted",)
    search_fields = ('title', )