from django.db import models
from django.utils.translation import gettext_lazy as _

class UserRegistrationMethods(models.TextChoices):
    email = "email", _("Email")
    google_oauth = "google_oauth", _("Google Oauth")



class TaskType(models.TextChoices):
    note = "note", _("note")
    checklist = "checklist", _("checklist")
    idea = "idea", _("idea")
    task = "task", _("task")

class TaskFragmentType(models.TextChoices):
    text = "text", _("text")
    image = "image", _("image")
    checkbox = "checkbox", _("checkbox")
    file = "file", _("file")